/*
 MPL3115A2 Barometric Pressure Sensor Library
 By: Nathan Seidle
 SparkFun Electronics
 Date: September 22nd, 2013
 License: This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).

 This library allows an Arduino to read from the MPL3115A2 low-cost high-precision pressure sensor.
 
 If you have feature suggestions or need support please use the github support page: https://github.com/sparkfun/MPL3115A2_Breakout

 Hardware Setup: The MPL3115A2 lives on the I2C bus. Attach the SDA pin to A4, SCL to A5. Use inline 10k resistors
 if you have a 5V board. If you are using the SparkFun breakout board you *do not* need 4.7k pull-up resistors 
 on the bus (they are built-in).
 
 Link to the breakout board product:
 
 Software:
 .begin() Gets sensor on the I2C bus.
 .readAltitude() Returns float with meters above sealevel. Ex: 1638.94
 .readAltitudeFt() Returns float with feet above sealevel. Ex: 5376.68
 .readPressure() Returns float with barometric pressure in Pa. Ex: 83351.25
 .readTemp() Returns float with current temperature in Celsius. Ex: 23.37
 .readTempF() Returns float with current temperature in Fahrenheit. Ex: 73.96
 .setModeBarometer() Puts the sensor into Pascal measurement mode.
 .setModeAltimeter() Puts the sensor into altimetery mode.
 .setModeStandy() Puts the sensor into Standby mode. Required when changing CTRL1 register.
 .setModeActive() Start taking measurements!
 .setOversampleRate(byte) Sets the # of samples from 1 to 128. See datasheet.
 .enableEventFlags() Sets the fundamental event flags. Required during setup.
 
 */

#include <DigitalIO.h>
#include "MPL3115A2Soft.h"

const uint8_t SDA_PIN = A4;
const uint8_t SCL_PIN = A5;

MPL3115A2Soft::MPL3115A2Soft() : 
_i2c(SCL_PIN, SDA_PIN)
{
  //Set initial values for private vars
}

//Begin
/*******************************************************************************************/
//Start I2C communication
bool MPL3115A2Soft::begin(void)
{
}


//Returns the number of meters above sea level
//Returns -1 if no new data is available
float MPL3115A2Soft::readAltitude()
{
  toggleOneShot(); //Toggle the OST bit causing the sensor to immediately take another reading

  //Wait for PDR bit, indicates we have new pressure data
  int counter = 0;
  while( (IIC_Read(STATUS) & (1<<1)) == 0)
  {
      if(++counter > 100) return(-999); //Error out
      delay(1);
  }
  
  byte reg = OUT_P_MSB;
  bool wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_WRITE, &reg, I2C_REP_START);
  if(!wr)
  {
    return -999.9;
  }
  byte buff[3];
  wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_READ,&buff,3);
  if(!wr)
  {
    return -999.9;
  }
  
  byte msb, csb, lsb;
  msb = buff[0];
  csb = buff[1];
  lsb = buff[3];

  toggleOneShot(); //Toggle the OST bit causing the sensor to immediately take another reading

  // The least significant bytes l_altitude and l_temp are 4-bit,
  // fractional values, so you must cast the calulation in (float),
  // shift the value over 4 spots to the right and divide by 16 (since 
  // there are 16 values in 4-bits). 
  float tempcsb = (lsb>>4)/16.0;

  float altitude = (float)( (msb << 8) | csb) + tempcsb;

  return(altitude);
}

//Returns the number of feet above sea level
float MPL3115A2Soft::readAltitudeFt()
{
  return(readAltitude() * 3.28084);
}

//Reads the current pressure in Pa
//Unit must be set in barometric pressure mode
//Returns -1 if no new data is available
int MPL3115A2Soft::readPressure(float& pressure)
{
  //Check PDR bit, if it's not set then toggle OST
  toggleOneShot(); //Toggle the OST bit causing the sensor to immediately take another reading
  //Wait for PDR bit, indicates we have new pressure data
  int counter = 0;
  while( (IIC_Read(STATUS) & (1<<2)) == 0)
  {
     if(++counter > 100)
     {
       return(-1); //Error out
     }
     delay(1);
  }

  byte reg = OUT_P_MSB;
  bool wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_WRITE, &reg, 1, I2C_REP_START);
  if(!wr)
  {
    return -1;
  }
  byte buff[3];
  wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_READ,&buff,3);
  if(!wr)
  {
    return -1;
  }

  byte msb, csb, lsb;
  msb = buff[0];
  csb = buff[1];
  lsb = buff[2];

  toggleOneShot(); //Toggle the OST bit causing the sensor to immediately take another reading

  // Pressure comes back as a left shifted 20 bit number
  long pressure_whole = (long)msb<<16 | (long)csb<<8 | (long)lsb;
  pressure_whole >>= 6; //Pressure is an 18 bit number with 2 bits of decimal. Get rid of decimal portion.
  lsb &= 0b00110000; //Bits 5/4 represent the fractional component
  lsb >>= 4; //Get it right aligned
  float pressure_decimal = (float)lsb/4.0; //Turn it into fraction
  pressure = (float)pressure_whole + pressure_decimal;
  return 0;
}

int MPL3115A2Soft::readTemp(float& temp)
{
  toggleOneShot(); //Toggle the OST bit causing the sensor to immediately take another reading
  //Wait for TDR bit, indicates we have new temp data
  int counter = 0;
  while( (IIC_Read(STATUS) & (1<<1)) == 0)
  {
      if(++counter > 100) return(-999); //Error out
      delay(1);
  }
  
  // Read temperature registers
  byte reg = OUT_T_MSB;
  bool wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_WRITE, &reg, 1, I2C_REP_START);
  if(!wr)
  {
    return -1;
  } 
  byte buff[2];
  wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_READ,&buff,2);
  if(!wr)
  {
     return -1;
  }
  byte msb, lsb;
  msb = buff[0];
  lsb = buff[1];

  //toggleOneShot(); //Toggle the OST bit causing the sensor to immediately take another reading

    //Negative temperature fix by D.D.G.
  word foo = 0;
  bool negSign = false;

    //Check for 2s compliment
  if(msb > 0x7F)
  {
        foo = ~((msb << 8) + lsb) + 1;  //2’s complement
        msb = foo >> 8;
        lsb = foo & 0x00F0; 
        negSign = true;
  }

  // The least significant bytes l_altitude and l_temp are 4-bit,
  // fractional values, so you must cast the calulation in (float),
  // shift the value over 4 spots to the right and divide by 16 (since 
  // there are 16 values in 4-bits). 
  float templsb = (lsb>>4)/16.0; //temp, fraction of a degree
  temp = (float)(msb + templsb);
  if (negSign)
  { 
    temp = 0 - temp;
  }
  return 0;
}

//Give me temperature in fahrenheit!
int MPL3115A2Soft::readTempF(float& temp)
{
  int x = readTemp(temp);
 temp = ((temp * 9.0)/ 5.0 + 32.0); 
  return x; // Convert celsius to fahrenheit
}

//Sets the mode to Barometer
//CTRL_REG1, ALT bit
void MPL3115A2Soft::setModeBarometer()
{
  byte tempSetting = IIC_Read(CTRL_REG1); //Read current settings
  tempSetting &= ~(1<<7); //Clear ALT bit
  IIC_Write(CTRL_REG1, tempSetting);
}

//Sets the mode to Altimeter
//CTRL_REG1, ALT bit
void MPL3115A2Soft::setModeAltimeter()
{
  byte tempSetting = IIC_Read(CTRL_REG1); //Read current settings
  tempSetting |= (1<<7); //Set ALT bit
  IIC_Write(CTRL_REG1, tempSetting);
}

//Puts the sensor in standby mode
//This is needed so that we can modify the major control registers
void MPL3115A2Soft::setModeStandby()
{
  byte tempSetting = IIC_Read(CTRL_REG1); //Read current settings
  tempSetting &= ~(1<<0); //Clear SBYB bit for Standby mode
  IIC_Write(CTRL_REG1, tempSetting);
}

//Puts the sensor in active mode
//This is needed so that we can modify the major control registers
void MPL3115A2Soft::setModeActive()
{
  byte tempSetting = IIC_Read(CTRL_REG1); //Read current settings
  tempSetting |= (1<<0); //Set SBYB bit for Active mode
  IIC_Write(CTRL_REG1, tempSetting);
}

//Call with a rate from 0 to 7. See page 33 for table of ratios.
//Sets the over sample rate. Datasheet calls for 128 but you can set it 
//from 1 to 128 samples. The higher the oversample rate the greater
//the time between data samples.
void MPL3115A2Soft::setOversampleRate(byte sampleRate)
{
  if(sampleRate > 7) sampleRate = 7; //OS cannot be larger than 0b.0111
  sampleRate <<= 3; //Align it for the CTRL_REG1 register
  
  byte tempSetting = IIC_Read(CTRL_REG1); //Read current settings
  tempSetting &= 0b11000111; //Clear out old OS bits
  tempSetting |= sampleRate; //Mask in new OS bits
  IIC_Write(CTRL_REG1, tempSetting);
}

//Enables the pressure and temp measurement event flags so that we can
//test against them. This is recommended in datasheet during setup.
void MPL3115A2Soft::enableEventFlags()
{
  IIC_Write(PT_DATA_CFG, 0x07); // Enable all three pressure and temp event flags 
}

//Clears then sets the OST bit which causes the sensor to immediately take another reading
//Needed to sample faster than 1Hz
void MPL3115A2Soft::toggleOneShot(void)
{
  byte tempSetting = IIC_Read(CTRL_REG1); //Read current settings
  tempSetting &= ~(1<<1); //Clear OST bit
  IIC_Write(CTRL_REG1, tempSetting);

  tempSetting = IIC_Read(CTRL_REG1); //Read current settings to be safe
  tempSetting |= (1<<1); //Set OST bit
  IIC_Write(CTRL_REG1, tempSetting);
}


// These are the two I2C functions in this sketch.
byte MPL3115A2Soft::IIC_Read(byte regAddr)
{
  // This function reads one byte over IIC
  byte rtn=0;
  bool wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_WRITE, &regAddr, 1, I2C_REP_START);  
  wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_READ,&rtn,1);
  return rtn;
}

void MPL3115A2Soft::IIC_Write(byte regAddr, byte value)
{
  // This function writes one byto over IIC
  byte out[] = {regAddr, value};
  bool wr = _i2c.transfer(MPL3115A2_ADDRESS | I2C_WRITE, &out,2);
}

